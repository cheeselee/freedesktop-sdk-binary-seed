#!/usr/bin/bash

. /usr/src/.env

set -e

unpack() {
  package="$1"
  stage="$2"
  test -z "$stage" && stage="0"
  archive="${SOURCES}/repo/${package}_${stage}.tar.bz2"

  echo "Unpacking ${archive}"
  tar xf "${archive}" -C /output
}

ls -l ${SOURCES}/repo

mkdir -p /output/usr/bin
ln -s bin /output/usr/sbin

# Extracted from init
unpack bzip2-1.0.8 0
unpack dhcpcd-9.4.1 0
unpack diffutils-2.7 0
unpack findutils-4.2.33 0
unpack flex-2.6.4 0
unpack gawk-3.0.4 0
unpack help2man-1.36.4 0
unpack linux-headers-5.10.41 0
unpack m4-1.4.7 0
unpack util-linux-2.19.1 0

# Extracted from run.sh
unpack bash-5.1

# Extracted from run2.sh
unpack xz-5.0.5
unpack libtool-2.4.7
unpack autoconf-2.69
unpack tar-1.34
unpack coreutils-8.32
unpack pkg-config-0.29.2
unpack make-4.2.1
unpack gmp-6.2.1
unpack autoconf-archive-2021.02.19
unpack mpfr-4.1.0
unpack mpc-1.2.1
unpack bison-3.4.2
unpack dist-3.5-236
unpack perl-5.32.1
unpack libarchive-3.5.2
unpack openssl-1.1.1l
unpack ca-certificates-3.86
unpack curl-7.83.0 1
unpack zlib-1.2.13
unpack automake-1.16.3
unpack autoconf-2.71
unpack patch-2.7.6
unpack gettext-0.21
unpack texinfo-6.7
unpack binutils-2.38
unpack gperf-3.1
unpack libunistring-0.9.10
unpack libffi-3.3
unpack libatomic_ops-7.6.10
unpack gc-8.0.4
unpack guile-3.0.7
unpack which-2.21
unpack grep-3.7
unpack sed-4.8
unpack autogen-5.18.16
unpack musl-1.2.3 1
unpack python-3.11.1
unpack gcc-10.4.0

# There is no package for this but glibc needs it
cp /usr/bin/gzip /output/usr/bin/gzip
