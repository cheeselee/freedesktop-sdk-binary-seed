# Copyright (c) 2023 Dor Askayo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Authors:
#       Dor Askayo <dor.askayo@gmail.com>

import contextlib
import os.path
import shutil
import urllib.error
import urllib.request

from buildstream import Source, SourceFetcher, SourceError
from buildstream import utils

class SourceManifestEntry(SourceFetcher):
    def __init__(self, source, url, sha256, directory, filename):
        super().__init__()

        self.source = source
        self.url = url
        self.sha256 = sha256
        self.directory = directory
        self.filename = filename

        self.mark_download_url(self.url)

    def _get_mirror_dir(self):
        return os.path.join(
            self.source.get_mirror_directory(),
            utils.url_directory_name(self.url),
        )

    def _get_mirror_filename(self):
        return os.path.join(self._get_mirror_dir(), self.sha256)

    def _ensure_mirror(self, url, activity_name: str):
        if self.is_cached():
            return

        with self.source.tempdir() as td:
            try:
                with self.source.timed_activity(activity_name):
                    file = _download_file(url, td, self.filename)

            except(
                urllib.error.HTTPError,
                urllib.error.URLError,
                urllib.error.ContentTooShortError,
                OSError,
                ValueError
            ) as e:
                raise SourceError("{}: Error downloading {}: {}".format(self, url, e), temporary=True) from e

            sha256 = utils.sha256sum(file)

            if sha256 != self.sha256:
                raise SourceError(
                    "File downloaded from {} has sha256sum '{}', not '{}'!".format(url, sha256, self.sha256)
                )

            utils.move_atomic(file, self._get_mirror_filename())

    def fetch(self, alias_override=None, **kwargs):
        url = self.get_translated_url(alias_override)
        self._ensure_mirror(url, "Fetching: {}".format(url))

    def stage(self, directory):
        try:
            dest = os.path.join(directory, self.directory, self.filename)

            with self.source.timed_activity("Staging source file to {}".format(dest)):
                os.makedirs(os.path.dirname(dest), exist_ok=True)
                utils.safe_copy(self._get_mirror_filename(), dest)
                os.chmod(dest, 0o644)

        except OSError as e:
            raise SourceError("{}: Error staging source: {}".format(self, e)) from e

    def get_translated_url(self, alias_override=None):
        return self.source.translate_url(self.url, alias_override=alias_override, primary=False)

    def is_cached(self):
        return os.path.isfile(self._get_mirror_filename())

    def is_resolved(self):
        return None not in (self.url, self.sha256, self.directory, self.filename)

def _download_file(url, directory, filename):
    request = urllib.request.Request(url)
    request.add_header("Accept", "*/*")
    request.add_header("User-Agent", "BuildStream/2")

    with contextlib.closing(urllib.request.urlopen(request)) as response:
        file = os.path.join(directory, filename)
        with utils.save_file_atomic(file, "wb") as dest:
            shutil.copyfileobj(response, dest)

    return file

class AbstractSourceManifest(Source):

    BST_MIN_VERSION = "2.0"

    def _create_manifest(self, ref):
        if ref is None:
            return []

        return [
            SourceManifestEntry(
                self,
                entry.get_str("url"),
                entry.get_str("sha256"),
                entry.get_str("directory"),
                entry.get_str("filename"),
            ) for entry in ref
        ]

    def _create_ref(self, manifest):
        return [{
            "url": entry.url,
            "sha256": entry.sha256,
            "directory": entry.directory,
            "filename": entry.filename,
        } for entry in manifest]

    def _update_ref(self, ref):
        self.manifest = self._create_manifest(ref)
        self.ref = self._create_ref(self.manifest)

    def _create_url_cache(self, manifest):
        url_cache = {}

        for entry in manifest:
          translated_url = entry.get_translated_url()
          url_cache[translated_url] = entry.url

        return url_cache

    def preflight(self):
        pass

    def is_resolved(self):
        return self.ref is not None and all(entry.is_resolved() for entry in self.manifest)

    def is_cached(self):
        return all(entry.is_cached() for entry in self.manifest)

    def load_ref(self, node):
        ref = node.get_sequence("ref", None)
        self._update_ref(ref)

        self.url_cache = self._create_url_cache(self.manifest)

    def get_ref(self):
        return self.ref

    def set_ref(self, ref, node):
        node["ref"] = ref
        self._update_ref(node.get_sequence("ref"))

    def stage(self, directory):
        for entry in self.manifest:
            entry.stage(directory)

    def get_source_fetchers(self):
        return self.manifest

    def get_cached_url(self, url):
        return self.url_cache.get(url, url)

class LiveBootstrapManifest(AbstractSourceManifest):

    BST_REQUIRES_PREVIOUS_SOURCES_TRACK = True

    SCRIPT_NAME = "source_manifest.py"

    def _get_manifest(self, directory):
        manifest_script = os.path.join(directory, LiveBootstrapManifest.SCRIPT_NAME)

        _, source_manifest = self.check_output(
            [self.host_python, manifest_script, "--system", *self.system.split()],
            cwd=directory,
            fail="Failed to get source manifest from {}".format(manifest_script)
        )

        return source_manifest.strip()

    def configure(self, node):
        self.system = node.get_str("system")

        node.validate_keys(Source.COMMON_CONFIG_KEYS + ["system", "ref"])

        self.load_ref(node)

    def get_unique_key(self):
        return [self.system, self.ref]

    def preflight(self):
        self.host_python = utils.get_host_tool("python3")

    def track(self, *, previous_sources_dir):
        source_manifest = self._get_manifest(previous_sources_dir)
        ref = []

        for line in source_manifest.split("\n"):
            sha256, directory, url, filename = line.strip().split(" ")

            ref.append({
                "url": self.get_cached_url(url),
                "sha256": sha256,
                "directory": directory,
                "filename": filename
            })

        return ref

def setup():
    return LiveBootstrapManifest
