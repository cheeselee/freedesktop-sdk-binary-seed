# Copyright (c) 2023 Seppo Yli-Olli
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Authors:
#       Seppo Yli-Olli <seppo.yliolli@gmail.com>

import os
import shutil

from buildstream import Source


class LiveBootstrapPrepare(Source):
    # pylint: disable=attribute-defined-outside-init

    BST_MIN_VERSION = "2.0"

    BST_REQUIRES_PREVIOUS_SOURCES_STAGE = True

    PLUGIN_VERSION = 3

    def configure(self, node):
        pass

    def preflight(self):
        pass

    def get_unique_key(self):
        return LiveBootstrapPrepare.PLUGIN_VERSION

    def is_resolved(self):
        return True

    def is_cached(self):
        return True

    def load_ref(self, node):
        pass

    def get_ref(self):
        return None

    def set_ref(self, ref, node):
        pass

    def fetch(self):
        pass

    def stage(self, directory):
        # See https://github.com/fosslinux/live-bootstrap#python-less-build
        stage0 = os.path.join(directory, "sysa/stage0-posix/src/")
        for item in os.listdir(stage0):
            source = os.path.join(stage0, item)
            target = os.path.join(directory, item)
            self.info(f"Moving {source} to {target}")
            os.rename(source, target)
        copy_map = {
            "sysa/after.kaem": "after.kaem"
        }
        for key, value in copy_map.items():
            source = os.path.join(directory, key)
            target = os.path.join(directory, value)
            self.info(f"Copying {source} to {target}")
            shutil.copy(source, target)
        os.unlink(os.path.join(directory, "sysc/after.sh"))

def setup():
    return LiveBootstrapPrepare
